package com.skripsi.agenda_user.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.skripsi.agenda_user.adapters.AgendaAdapter;
import com.skripsi.agenda_user.R;
import com.skripsi.agenda_user.activities.HomeActivity;
import com.skripsi.agenda_user.adapters.ChatAdapter;
import com.skripsi.agenda_user.helers.MyDate;
import com.skripsi.agenda_user.helers.Util;
import com.skripsi.agenda_user.models.ChatModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatFragment extends Fragment {

    @BindView(R.id.listChat)
    RecyclerView listChat;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    @BindView(R.id.et_message)
    EditText edMsg;
    @BindView(R.id.icSend)
    ImageView icSend;
    private DatabaseReference roomRef;
    private HomeActivity homeActivty;
    private ChatAdapter chatAdapter;
    private AgendaAdapter agendaAdapter;
    private Query mRef;
    private String idRoom = "admin-0";

    public static ChatFragment newInstance(String roomID) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString("roomID", roomID);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment, container, false);
        ButterKnife.bind(this, view);
        homeActivty = (HomeActivity) getActivity();
        if (getArguments() != null) {
            idRoom = getArguments().getString("roomID");
        }
        chatAdapter = new ChatAdapter(getContext());
        listChat.setLayoutManager(new LinearLayoutManager(getContext()));
        listChat.setAdapter(chatAdapter);

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("chat");
        roomRef = databaseReference.child(idRoom);
        mRef = roomRef.orderByKey().limitToLast(50);
        mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ChatModel messages = dataSnapshot.getValue(ChatModel.class);
                chatAdapter.add(messages);
                chatAdapter.notifyDataSetChanged();
                listChat.scrollToPosition(chatAdapter.getItemCount() - 1);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        isTyping(false);
        edMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty() && s.length() > 0 && !s.equals("")) {
                    isTyping(true);
                } else {
                    isTyping(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        icSend.setOnClickListener(v -> {
            if (!edMsg.getText().toString().trim().isEmpty()) addMessage();
        });

        return view;
    }

    private void addMessage() {
        ChatModel mData = new ChatModel();
        mData.setName("user1");
        mData.setMessage(edMsg.getText().toString());
        mData.setTime(MyDate.getCurDateTime());
        mData.setStatus("user");
        roomRef.push().setValue(mData);
        edMsg.setText("");
        Util.hideSoftKeyboard(homeActivty);
    }

    private void isTyping(boolean typing) {
        icSend.setColorFilter(typing ? ContextCompat.getColor(homeActivty, R.color.colorPrimary) : ContextCompat.getColor(homeActivty, R.color.colorDarkGray), android.graphics.PorterDuff.Mode.MULTIPLY);
        icSend.setEnabled(typing);
    }



}
