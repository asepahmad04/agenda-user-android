package com.skripsi.agenda_user.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.skripsi.agenda_user.R;
import com.skripsi.agenda_user.helers.Util;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edEmail) TextView edEmail;
    @BindView(R.id.edPass) TextView edPass;
    @BindView(R.id.tvDeskripsi) TextView tvDeskripsi;
    @BindView(R.id.btnLogin) Button btnLogin;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        btnLogin.setOnClickListener(v -> {
            String email = edEmail.getText().toString().trim();
            String password = edPass.getText().toString().trim();
            if(email.isEmpty()){
                edEmail.setError("Email harus diisi");
                edEmail.requestFocus();
                return;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                edEmail.setError("Masukkan email dengan benar");
                edEmail.requestFocus();
                return;
            }
            if(password.isEmpty()){
                edPass.setError("Password harus diisi");
                edPass.requestFocus();
                return;
            }
            if(password.length() < 6){
                edPass.setError("Minimal 6 karakter");
                edPass.requestFocus();
                return;
            }
            progressBar.setVisibility(View.VISIBLE);
            actLogin(email, password);
        });

        tvDeskripsi.setOnClickListener(v -> startActivity(new Intent(this, RegisterActivity.class)));
    }

    private void actLogin(String email, String password) {
        Util.hideSoftKeyboard(this);
        progressBar.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            progressBar.setVisibility(View.GONE);
            if(task.isSuccessful()) {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                String error = "";
                try {
                    throw Objects.requireNonNull(task.getException());
                }catch(FirebaseAuthInvalidCredentialsException e) {
                    error = "Password salah";
                }catch(Exception e) {
                    error = "Kesalahan/email belum terdaftar";
                }
                Util.showToast(LoginActivity.this, error);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mAuth.getCurrentUser() !=null) {
            finish();
            startActivity(new Intent(this,HomeActivity.class));
        }
    }
}
