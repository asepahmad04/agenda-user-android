package com.skripsi.agenda_user.helers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Util {

    public static void showToast(Context activity, String message){
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static ProgressDialog showProgressDialog(Context context, String message){
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        return dialog;
    }

    public static String getAddressFromLocation(Activity mActivity, double lat, double lng) {
        Geocoder gcd = new Geocoder(mActivity, Locale.getDefault());
        List<Address> addresses = null;
        String strAddress = null;
        try {
            addresses = gcd.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                strAddress = addresses.get(0).getAddressLine(0) + ", " + addresses.get(0).getLocality();
            } else {
                // do your staff
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strAddress;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getDateCurrent(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
