package com.skripsi.agenda_user.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.skripsi.agenda_user.R;

import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
    }
}
